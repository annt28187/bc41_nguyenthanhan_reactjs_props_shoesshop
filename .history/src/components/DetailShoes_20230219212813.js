import React, { Component } from 'react';

export default class DetailShoes extends Component {
  render() {
    let { name, price, description, image } = this.props.detail;
    return (
      <div>
        <h2>Detail Shoes</h2>
        <div className="row mt-5 alert-secondary p-5 text-left">
          <img className="col-3" src={image} alt={name} />
          <div className="col-9">
            <h5>{name}</h5>
            <p>{description}</p>
            <p>{price} $</p>
          </div>
        </div>
      </div>
    );
  }
}
