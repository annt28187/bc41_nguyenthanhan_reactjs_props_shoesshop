import React, { Component } from 'react';
import CartShoes from './CartShoes';
import { dataShoes } from './data_shoes';
import DetailShoes from './DetailShoes';
import ListShoes from './ListShoes';

export default class ShoesShop extends Component {
  state = {
    listShoes: dataShoes,
  };
  render() {
    return (
      <div>
        <h2>Shoes Shop</h2>
        <CartShoes />
        <ListShoes list={this.state.listShoes} />
        <DetailShoes />
      </div>
    );
  }
}
