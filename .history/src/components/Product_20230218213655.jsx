import React, { Component } from 'react';

export default class Product extends Component {
  renderProduct = () => {
    let title = 'Alex';
    let production = `<p><b>FrontEnd Master</b></p>`;
    return (
      <div>
        {title} <br /> {production}
      </div>
    );
  };
  render() {
    return <div className="Product">{this.renderProduct()}</div>;
  }
}
