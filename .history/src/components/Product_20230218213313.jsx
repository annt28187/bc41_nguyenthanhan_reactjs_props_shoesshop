import React, { Component } from 'react';

export default class Product extends Component {
  render() {
    let title = 'Alex';
    let production = `<p><b>FrontEnd Master</b></p>`;
    return (
      <div className="Product">
        {title} {production}
      </div>
    );
  }
}
