import React, { Component } from 'react';

export default class CartShoes extends Component {
  renderTbody = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <img src={item.image} style={{ width: 50 }} alt={item.name} />
          </td>
          <td>{item.price}</td>
          <td>
            <button className="btn btn-warning">Delete</button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <th>ID</th>
            <th>Name</th>
            <th>Image</th>
            <th>Price</th>
            {/* <th>Quantity</th> */}
            <th>Action</th>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}
