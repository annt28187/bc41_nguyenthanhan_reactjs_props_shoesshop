import React, { Component } from 'react';
import ItemShoes from './ItemShoes';

export default class ListShoes extends Component {
  renderListShoes = () => {
    return this.props.list.map((item, index) => {
      return (
        <ItemShoes key={index} shoes={item} handleClick={this.props.handleChangeDetailShoes} />
      );
    });
  };
  render() {
    return <div className="row">{this.renderListShoes()}</div>;
  }
}
