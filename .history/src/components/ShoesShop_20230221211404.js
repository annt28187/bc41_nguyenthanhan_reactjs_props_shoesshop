import React, { Component } from 'react';
import CartShoes from './CartShoes';
import { dataShoes } from './data_shoes';
import DetailShoes from './DetailShoes';
import ListShoes from './ListShoes';

export default class ShoesShop extends Component {
  state = {
    listShoes: dataShoes,
    detail: dataShoes[0],
    cart: [],
  };

  handleChangeDetailShoes = (shoes) => {
    this.setState({
      detail: shoes,
    });
  };

  handleAddToCart = (shoes) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id === shoes.id;
    });
    if (index === -1) {
      let cartItem = { ...shoes, number: 1 };
      cloneCart.push(cartItem);
    } else {
      cloneCart[index].number++;
    }
  };

  render() {
    return (
      <div className="container">
        <h2>Shoes Shop</h2>
        <CartShoes cart={this.state.cart} />
        <ListShoes
          list={this.state.listShoes}
          handleChangeDetailShoes={this.handleChangeDetailShoes}
          handleAddToCart={this.handleAddToCart}
        />
        <DetailShoes detail={this.state.detail} />
      </div>
    );
  }
}
