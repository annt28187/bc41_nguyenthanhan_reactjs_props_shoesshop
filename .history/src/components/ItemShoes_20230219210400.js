import React, { Component } from 'react';

export default class ItemShoes extends Component {
  render() {
    let { image, name, price } = this.props.shoes;
    return (
      <div className="col-3 p-4">
        <div className="card">
          <img className="card-img-top" src={image} alt={name} />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <p className="card-text">{price}</p>
          </div>
        </div>
      </div>
    );
  }
}
