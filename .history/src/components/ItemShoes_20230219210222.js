import React, { Component } from 'react';

export default class ItemShoes extends Component {
  render() {
    let { image, name, price } = this.props.shoes;
    return (
      <div className="col-3 p-4">
        <div className="card">
          <img className="card-img-top" src="holder.js/100px180/" alt />
          <div className="card-body">
            <h4 className="card-title">Title</h4>
            <p className="card-text">Body</p>
          </div>
        </div>
      </div>
    );
  }
}
