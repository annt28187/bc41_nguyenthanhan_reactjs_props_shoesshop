import React, { Component } from 'react';

export default class DetailShoes extends Component {
  render() {
    let { id, name, price, description, image } = this.props.detail;
    return (
      <div>
        <h2>Detail Shoes</h2>
        <div className="row mt-5 alert-secondary p-5 text-left">
          <img src={image} alt={name} className="col-3" />
          <div className="col-9">
            <p>ID: {id}</p>
            <h5>Name: {name}</h5>
            <p>Desc: {description}</p>
            <p>Price: {price}</p>
          </div>
        </div>
      </div>
    );
  }
}
