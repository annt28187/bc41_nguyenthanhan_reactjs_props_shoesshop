import React, { Component } from 'react';
import ItemShoes from './ItemShoes';

export default class ListShoes extends Component {
  renderListShoes = () => {
    return this.props.list.map((item, index) => {
      return (
        <ItemShoes
          key={index}
          shoes={item}
          handleClickDetail={this.props.handleChangeDetailShoes}
          handleClickCart={this.props.handleClickCart}
        />
      );
    });
  };
  render() {
    return <div className="row">{this.renderListShoes()}</div>;
  }
}
