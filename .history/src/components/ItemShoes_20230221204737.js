import React, { Component } from 'react';

export default class ItemShoes extends Component {
  render() {
    let { image, name, price } = this.props.shoes;
    return (
      <div className="col-3 p-4">
        <div className="card border-primary h-100">
          <img className="card-img-top" src={image} alt={name} />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p className="card-text">{price} $</p>
            <button className="btn btn-primary mr-2">Add to cart</button>
            <button
              className="btn btn-secondary"
              onClick={() => {
                this.props.handleClick(this.props.shoes);
              }}
            >
              Show Detail
            </button>
          </div>
        </div>
      </div>
    );
  }
}
