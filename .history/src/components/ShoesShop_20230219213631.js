import React, { Component } from 'react';
import CartShoes from './CartShoes';
import { dataShoes } from './data_shoes';
import DetailShoes from './DetailShoes';
import ListShoes from './ListShoes';

export default class ShoesShop extends Component {
  state = {
    listShoes: dataShoes,
    detail: dataShoes[0],
    cart: [],
  };
  render() {
    return (
      <div>
        <h2>Shoes Shop</h2>
        <CartShoes cart={this.state.cart} />
        <ListShoes list={this.state.listShoes} />
        <DetailShoes detail={this.state.detail} />
      </div>
    );
  }
}
